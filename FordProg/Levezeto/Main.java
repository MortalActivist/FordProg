import java.util.Scanner;

public class Main {

    public static void levezetes(String bemenetSzo){

        String kezdoSzo="S#";
        String jelzes="";

        while(true)
        {
            if(kezdoSzo.charAt(0)=='S'){
                kezdoSzo=kezdoSzo.replace("S","ABC");
                jelzes+="1";
            }

            else if(kezdoSzo.charAt(0)=='A' && bemenetSzo.charAt(0)=='a'){
                kezdoSzo=kezdoSzo.replaceFirst("A","a");
                jelzes+="2";
            }

            else if(kezdoSzo.charAt(0)=='A' && (bemenetSzo.charAt(0)=='b' || bemenetSzo.charAt(0)=='c')){
                kezdoSzo=kezdoSzo.replaceFirst("A","Bbc");
                jelzes+="3";
            }

            else if(kezdoSzo.charAt(0)=='A' && (bemenetSzo.charAt(0)=='d' || bemenetSzo.charAt(0)=='e')) {
               kezdoSzo=kezdoSzo.replaceFirst("A","Ccd");
               jelzes+="4";
            }

            else if(kezdoSzo.charAt(0)=='B' && bemenetSzo.charAt(0)=='b'){
                kezdoSzo=kezdoSzo.replaceFirst("B","bBb");
                jelzes+="5";
            }

            else if(kezdoSzo.charAt(0)=='B' && bemenetSzo.charAt(0)=='c'){
                kezdoSzo=kezdoSzo.replaceFirst("B","cCc");
                jelzes+="6";
            }

            else if(kezdoSzo.charAt(0)=='C' && bemenetSzo.charAt(0)=='d'){
                kezdoSzo=kezdoSzo.replaceFirst("C","dDd");
                jelzes+="7";
            }

            else if(kezdoSzo.charAt(0)=='C' && bemenetSzo.charAt(0)=='e'){
                kezdoSzo=kezdoSzo.replaceFirst("C","Dd");
                jelzes+="8";
            }
            else if(kezdoSzo.charAt(0)=='D' && bemenetSzo.charAt(0)=='e'){
                kezdoSzo=kezdoSzo.replaceFirst("D","e");
                jelzes+="9";
            }
            else if(kezdoSzo.charAt(0)==bemenetSzo.charAt(0)){
                kezdoSzo=kezdoSzo.substring(1);
                bemenetSzo=bemenetSzo.substring(1);
            }
            else {
                System.out.println("A szó nem része a nyelvnek.");
                System.out.println(jelzes);
                break;
            }

            if(bemenetSzo.charAt(0)=='#' && kezdoSzo.charAt(0)=='#'){
                System.out.println("A szó a nyelv része.");
                System.out.println("A következő sorozattal vezethető le: ");
                System.out.println(jelzes);
                break;
            }

        }


    }

    public static void main (String args[]){

        Scanner scanner=new Scanner(System.in);
        String bemenetSzo=scanner.nextLine();

        bemenetSzo+="#";
        levezetes(bemenetSzo);

        scanner.close();




    }
}
