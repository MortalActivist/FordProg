import java.util.Scanner;

public class Main{

    public static boolean isNumber (char sign) {
        //Függvény ami arra szolgál, hogy eldöntsük, szám e a karakter

        char[] szamTomb = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

        //megnézzük szám-e a karakter, tömbben keresünk egyezést
        for (int i = 0; i < szamTomb.length; i++) {

            if (sign == szamTomb[i]) {
                return true;
            }
        }

        return false;

    }

    public static boolean isLetter(char sign){
        //Arra szolgál hogy eldöntsük, betű-e a karakter

        char[] abc = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
                's', 't', 'u', 'v', 'w', 'x', 'y',
                'z','á','é','í','ó','ö','ő','ú','ü','ű'};

        //Tömbön belül folytatunk keresést
        for (int i = 0; i < abc.length; i++) {

            if (sign == abc[i]) {
                return true;
            }
        }

        return false;

    }

//Eldöntjük a bemenetről, hogy mikből áll
    public static void decide (String bemenetSzo){

        String jelzes="";
        boolean azonositoban=false;
        boolean kommentben=false;

        //Végigmegyünk a parancson karakterenként
        for(int i=0; i<bemenetSzo.length(); i++){

            //Hibakezelés kommentekre
            if((bemenetSzo.charAt(i)=='}') && kommentben==false) {
                jelzes += "<hiba>";
            }

            if(bemenetSzo.charAt(i)==')' && kommentben==false){
               jelzes+="<hiba>";
            }

            //Hiba
            if(bemenetSzo.charAt(i)=='*' && kommentben==false){
                jelzes+="<hiba>";
            }

            //Hibakezelés értékkezelésnél
            if(bemenetSzo.charAt(i)=='=' ){
                jelzes+="<hiba>";
            }

            //Ha nincs a végén $ jel, akkor hibás és leáll a program
            if (bemenetSzo.charAt(bemenetSzo.length()-1)!='$'){
                System.out.println("Hibas parancs.");
                break;
            }

            //Ha számról van szó és nem azonosítóban van vagy kommentben, akkor megjegyezzük
            if(isNumber(bemenetSzo.charAt(i)) && azonositoban==false && kommentben==false){
                jelzes+="<szam>";
                while(isNumber(bemenetSzo.charAt(i))){
                    i++;
                }
            }

            //Ha betűről van szó és nem kommentben van, akkor megjegyezzük
            if(isLetter(bemenetSzo.charAt(i)) && kommentben==false){
                jelzes+="<azonosito>";
                azonositoban=true;
                //Addig megyünk az azonosítóban, amíg nem találunk neki egy végét. Tehát amíg szám vagy betű van benne
                while(isLetter(bemenetSzo.charAt(i+1))==true || isNumber(bemenetSzo.charAt(i+1))==true){
                    i++;
                }
                azonositoban=false;

            }

            //Komment megfigyelése: { }
            if(bemenetSzo.charAt(i)=='{'){
                kommentben=true;
                while(bemenetSzo.charAt(i)!='}'){
                    i++;
                    if(bemenetSzo.charAt(i)=='$'){
                        jelzes+="<hiba>";
                    }
                    if(bemenetSzo.charAt(i+1)=='}'){
                        jelzes+="< {} komment>";
                        kommentben=false;
                        i++;
                        break;
                    }
                }
            }

            //Komment megfigyelése: (**)
            if(bemenetSzo.charAt(i)=='(') {
                if (bemenetSzo.charAt(i + 1) == '*') {
                    kommentben = true;
                    i += 2;
                    while (kommentben) {
                        if (bemenetSzo.charAt(i) == '*' && bemenetSzo.charAt(i + 1) == ')') {
                            jelzes += "<* kommentben>";
                            i++;
                            kommentben=false;
                            break;
                        }
                        if (bemenetSzo.charAt(i) == '$') {
                            jelzes += "<hiba>";
                            break;
                        }
                            i++;
                    }
                }
            }

            //Értékadás
            if(bemenetSzo.charAt(i)==':'){
                if(bemenetSzo.charAt(i+1)=='='){
                    jelzes+="<értékadás>";
                    i++;
                }
                else{
                    jelzes+="<hiba>";
                }
            }

            if(bemenetSzo.charAt(i)=='<'){
                if(bemenetSzo.charAt(i+1)=='='){
                    jelzes+="< <= >";
                    i++;
                }
                else{
                    jelzes+="<hiba>";
                }
            }

            if(bemenetSzo.charAt(i)=='>'){
                if(bemenetSzo.charAt(i+1)=='='){
                    jelzes+="< >= >";
                    i++;
                }
                else{
                    jelzes+="<hiba>";
                }
            }

            if(bemenetSzo.charAt(i)=='<'){
                if(bemenetSzo.charAt(i+1)=='>'){
                    jelzes+="< <> >";
                    i++;
                }
            }

        }


        System.out.println(jelzes);

    }

    public static void main (String args[]) {

        Scanner scanner = new Scanner (System.in);
        String bemenetSzo=scanner.nextLine();

        bemenetSzo=bemenetSzo.toLowerCase();
        decide(bemenetSzo);

        scanner.close();

    }
}
